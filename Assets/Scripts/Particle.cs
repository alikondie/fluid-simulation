﻿/* This Script is inspired from the paper by Simon Clavet,Philippe Beaudoin,and Pierre Poulin Particle-based Viscoelastic Fluid Simulation
 http://www.ligum.umontreal.ca/Clavet-2005-PVFS/pvfs.pdf
 */




using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle : MonoBehaviour {

    // Use this for initialization
    public float delta;
    float gravity = -9.8f;
    float masse;
    //float initialSpeedX;
    Vector3 velocity = new Vector3(0, 0, 0);
    Vector3 vitesse;
    Vector3 previousPos;
    SpriteRenderer r;
	void Start () {

        masse = Random.Range(10f, 10f);
        velocity.y = Random.Range(-6f, 6f);
        velocity.x = Random.Range(-6f, 6f);
        r = GetComponent<SpriteRenderer>(); 
    }
	
	// Update is called once per frame
	void Update () {


        r.color = new Color(1f, 1f - masse / 10, 1f - masse / 10);
        delta =  Time.deltaTime;
        // Vi = Vi + delta * gravity
        
        // xiPrev = xia
        
        previousPos = transform.localPosition;
        // xi = xi + deltaT*Vi
        velocity.y += delta * gravity;
        transform.localPosition = new Vector3(transform.localPosition.x + velocity.x * delta, velocity.y * delta + transform.localPosition.y, transform.localPosition.z);
        if (transform.localPosition.y - -6.0f <= 0)
        {
            velocity.y = vitesse.y + delta * (previousPos.y * (-10f) - 10f * delta * vitesse.y) / masse;
            transform.localPosition = new Vector3(transform.localPosition.x + velocity.x * delta, previousPos.y + velocity.y * delta, transform.localPosition.z);
            
        }
        if (transform.localPosition.x - 4.0f >= 0)
        {
            velocity.x = vitesse.x + delta * (previousPos.x * (-100f) - 100f * delta * vitesse.x) / masse;
            transform.localPosition = new Vector3(previousPos.x + velocity.x * delta, transform.localPosition.y + velocity.y * delta, transform.localPosition.z);
            
        }
        if (transform.localPosition.x - -4.0f <= 0)
        {
            velocity.x = vitesse.x + delta * (previousPos.x * (-100f) - 100f * delta * vitesse.x) / masse;
            transform.localPosition = new Vector3(previousPos.x + velocity.x * delta, transform.localPosition.y + velocity.y * delta, transform.localPosition.z);
            
        }


        vitesse = (transform.localPosition - previousPos) * delta;
	}



    
}
