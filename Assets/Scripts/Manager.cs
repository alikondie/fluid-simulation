﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {

    // Use this for initialization
    public GameObject particle;
    public Transform createPosition;
    List<GameObject> particles;
    float sceneWidth, sceneHeight;
    //float cellsize;
    int nParticles = 0;
    public float k = 5;
    public float ro0 = 1f;
    Dictionary<int, List<GameObject>> cases = new Dictionary<int, List<GameObject>>();
    void Start () {
        particles = new List<GameObject>();
        sceneWidth = 8;
        sceneHeight = 12;

        for(int i = 0; i < sceneHeight * sceneWidth; i++)
        {
            cases.Add(i, new List<GameObject>());
        }

    }
    void clearCases()
    {
        cases.Clear();
        for (int i = 0; i < sceneHeight * sceneWidth; i++)
        {
            cases.Add(i, new List<GameObject>());
        }
    }
    private void AddToList(Vector3 vector, float width, List<GameObject> buckettoaddto)
    {
        int cellPosition = (int)(
                   (Mathf.Floor(vector.x)) +
                   (Mathf.Floor(vector.y)) *
                   width
        );
     /*if (!buckettoaddto.Contains(cellPosition))
            buckettoaddto.Add(cellPosition);*/

    }
    List<GameObject> getObjectIndex(GameObject o)
    {
        List<int> l = new List<int>();
        Vector3 pos = new Vector3(o.transform.position.x, o.transform.position.y, 0);
        return null;
    }

    void EnterObject(GameObject o)
    {
        
    }
	
	// Update is called once per frame
	void Update () {
        //StartCoroutine("createParticle");
        nParticles++;
        if(nParticles < 100) { 
            particle = Instantiate(particle, new Vector3(createPosition.position.x, createPosition.position.y, createPosition.position.z), Quaternion.identity);
            particles.Add(particle);
        }

        densityCheck();

    }
    void FixedUpdate()
    {
        
    }

    void densityCheck()
    {
        
        float distanceH = 2f;
        List<GameObject> neighbors = new List<GameObject>();
        foreach (GameObject particle in particles)
        {
            float ro = 0;
            float roNear = 0;

            foreach(GameObject p in particles)
            {
               float q = Vector3.Distance(particle.transform.position, p.transform.position) / distanceH;
               if(q < 1)
                {
                    neighbors.Add(p);
                    ro = ro + (1 - q) * (1 - q);
                    roNear = roNear + (1 - q) * (1 - q) * (1 - q);
                }
            }
            
            float kNear = 5f;
            
            float pressure = k * (ro - ro0);
            float pressureNear = kNear * roNear; 
            float dxx = 0;
            float dxy = 0;
            float Dx = 0;
            float Dy = 0;
            foreach (GameObject p in particles)
            {
                Vector3 rChapeau = (particle.transform.position - p.transform.position).normalized ;
                float q = Vector3.Distance(particle.transform.position, p.transform.position) / distanceH;
                if (q < 1)
                {
                    float delta = particle.GetComponent<Particle>().delta;
                    Dx = delta * delta * (pressure * (1 - q)); //+ roNear * (1-q)*(1-q) * rChapeau.x;
                    Dy = delta * delta * (pressure * (1 - q)); //+ roNear * (1-q)*(1-q) * rChapeau.y;
                    //Debug.Log(Dx);
                    //Debug.Log(Dy);
                    p.transform.localPosition = new Vector3(p.transform.position.x + Dx/2,p.transform.position.y + Dy/2,p.transform.position.y);
                    dxx = dxx - Dx / 2;
                    dxy = dxy - Dy / 2;
                    //Debug.Log(dxx);
                    //Debug.Log(dxy);
                }
            }
            particle.transform.localPosition = new Vector3(particle.transform.position.x + dxx, particle.transform.position.y - dxy, particle.transform.position.z);
        }

    }

    IEnumerator createParticle()
    {
        
        yield return new WaitForSeconds(1f);
        Instantiate(particle, new Vector3(createPosition.position.x, createPosition.position.y, createPosition.position.z),Quaternion.identity);


    }
}
